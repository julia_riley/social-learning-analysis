This is the information file for the code used in the paper **"Learning ability is unaffected by isolation rearing in a family-living lizard"** published in Behavioural Ecology and Sociobiology (DOI: 10.1007/s00265-017-2435-9). 


Any comments or questions, please contact Julia Riley: julia.riley87@gmail.com


The following files are provided in the 'data' folder:

1. *all_trials_by_liz.csv* - attribute data (sex, sex measurements, number of trials to learn, etc.) for all lizards in each task
2. *interobserver_bias.csv* - behavioural scoring from all markers to assess inter-observer bias
3. *instru_by_trial.csv* - data from the instrumental task for each trial
4. *assoc_by_trial.csv* - data from the association task for each trial
5. *reverse_by_trial.csv* - data from the reversal task for each trial


To run the analysis and plot the graphs provided in the paper, use the files in the �script� folder: 

1. *interobserver_bias.R* - code for our examination of behavioural score agreement between markers
2. *ES_analysis_instru.R* - code for the instrumental task analyses
3. *ES_analysis_assoc.R* - code for the assocation task analyses
4. *ES_analysis_reverse.R* - code for the reversal task analyses
5. *figure1.R* - code to generate Figure.1 for the manuscript

These additional files are within the main repository:

1. *Analysis.Rproj* - the Rstudio project for this analyses
2. *.gitignore* - the ignore file